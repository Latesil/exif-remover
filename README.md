# **THIS REPOSITORY MOVED TO https://github.com/Latesil/exif-remover**


# EXIF Remover

<p align="center">
  <img src="https://gitlab.com/Latesil/exif-remover/-/raw/master/exif-remover-screenshot-1.png" style="max-width:100%;">
</p>

EXIF Remover is a program for remove all EXIF metadata from your photos.
